let sStoIni = function (tar) {
  if (tar !== undefined) {
    sStoIniTag(tar);
  }
};

let sStoGetLin = function (ele) {
  let par = ele.parentElement;
  let num = ele.getAttribute('s-sto-num');
  return par.querySelector('.s-sto-lin[s-sto-num="' + num + '"]');
};

let sStoIniTag = function (tag) {
  let lin = sStoGetLin(tag);
  if (tag.scrollHeight === tag.clientHeight) {
    lin.classList.remove('s-sto-on');
    lin.classList.remove('s-scr-top');
    lin.classList.remove('s-scr-bot');
    return;
  }
  if (tag.scrollTop) {
    lin.classList.add('s-scr-top');
  }
  if (tag.scrollTop === 0) {
    lin.classList.add('s-scr-bot');
  }
  lin.classList.add('s-sto-on');
};
let sStoNum = 0;
let sStoGetSto = function (ele) {
  let num = ele.getAttribute('s-sto-num');
  let par = ele.parentElement;
  return par.querySelector('.s-sto[s-sto-num="' + num + '"]');
};

let sStolinCli = function (eve) {
  eve.preventDefault();
  let sto = sStoGetSto(this);
  if (this.classList.contains('s-scr-bot')) {
    sto.scrollTop = sto.scrollHeight - sto.clientHeight;
    this.classList.remove('s-scr-bot');
    this.classList.add('s-scr-top');
    return false;
  }
  if (this.classList.contains('s-scr-top')) {
    sto.scrollTop = 0;
    this.classList.remove('s-scr-top');
    this.classList.add('s-scr-bot');
    return false;
  }
};
let sStoAdd = function (eve) {
  if (eve.animationName !== 'sStoAdd') {
    return;
  }
  let lin = document.createElement('a');
  lin.href = '#';
  lin.setAttribute('s-sto-num', sStoNum);
  eve.target.setAttribute('s-sto-num', sStoNum);
  sStoNum++;
  lin.classList.add('s-sto-lin');
  lin.addEventListener("click", sStolinCli);
  eve.target.parentElement.classList.add('s-sto-wr');
  eve.target.after(lin);
  let top = eve.target.offsetTop;
  lin.style.top = top + 'px';
  let lef = eve.target.offsetLeft;
  lin.style.left = lef + 'px';
  eve.target.onmouseover = function (event) {
    lin.classList.add('s-sto-sho');
  };
  lin.onmouseover = function (event) {
    lin.classList.add('s-sto-sho');
  };
  eve.target.onmouseout = function (event) {
    lin.classList.remove('s-sto-sho');
  };
  lin.onmouseout = function (event) {
    lin.classList.remove('s-sto-sho');
  };
  sStoIni(eve.target);
};
if (document.sStoAddReg === undefined) {
  document.addEventListener("animationstart", sStoAdd);
  document.addEventListener("MSAnimationStart", sStoAdd);
  document.addEventListener("webkitAnimationStart", sStoAdd);
  window.addEventListener('resize', sStoIni);
  window.addEventListener('orientationChange', sStoIni);
  document.sStoAddReg = true;
}
